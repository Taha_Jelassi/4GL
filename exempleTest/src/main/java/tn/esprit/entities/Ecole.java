package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Ecole
 *
 */
@Entity

public class Ecole implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idEcole;
	private String nom;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "ecole")
	private List<DateEnseignement> lstdateens;
	@OneToMany
	private List<Salle> lstsalle;

	public long getIdEcole() {
		return idEcole;
	}

	public void setIdEcole(long idEcole) {
		this.idEcole = idEcole;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Ecole() {
		super();
	}

}
