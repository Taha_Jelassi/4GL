package tn.esprit.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Enseignant
 *
 */
@Entity

public class Enseignant implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idEns;
	private String name;
	private String lastname;
	@Enumerated(EnumType.STRING)
	private TypeEns typeens;
	@OneToMany(mappedBy = "enseignant")
	private List<Cour> lstcours;
	@OneToMany(mappedBy = "ens")
	private List<DateEnseignement> lstdateens;
	@OneToOne(mappedBy = "ensgn")
	private Salle salle;

	public List<DateEnseignement> getLstdateens() {
		return lstdateens;
	}

	public void setLstdateens(List<DateEnseignement> lstdateens) {
		this.lstdateens = lstdateens;
	}

	public List<Cour> getLstcours() {
		return lstcours;
	}

	public void setLstcours(List<Cour> lstcours) {
		this.lstcours = lstcours;
	}

	public TypeEns getTypeens() {
		return typeens;
	}

	public void setTypeens(TypeEns typeens) {
		this.typeens = typeens;
	}

	public long getIdEns() {
		return idEns;
	}

	public void setIdEns(long idEns) {
		this.idEns = idEns;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	private static final long serialVersionUID = 1L;

	public Enseignant() {
		super();
	}

}
