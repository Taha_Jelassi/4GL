package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: DateEnsPk
 *
 */
@Embeddable

public class DateEnsPk implements Serializable {

	private long idens;
	private long idecole;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idecole ^ (idecole >>> 32));
		result = prime * result + (int) (idens ^ (idens >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateEnsPk other = (DateEnsPk) obj;
		if (idecole != other.idecole)
			return false;
		if (idens != other.idens)
			return false;
		return true;
	}

	public long getIdens() {
		return idens;
	}

	public void setIdens(long idens) {
		this.idens = idens;
	}

	public long getIdecole() {
		return idecole;
	}

	public void setIdecole(long idecole) {
		this.idecole = idecole;
	}

	private static final long serialVersionUID = 1L;

	public DateEnsPk() {
		super();
	}

}
