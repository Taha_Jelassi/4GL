package tn.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Cour
 *
 */
@Entity

public class Cour implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idCour;
	private String seance;
	private Date jour;
	@ManyToOne
	private Enseignant enseignant;
	@ManyToOne
	private Salle salle;

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public long getIdCour() {
		return idCour;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	public void setIdCour(long idCour) {
		this.idCour = idCour;
	}

	public String getSeance() {
		return seance;
	}

	public void setSeance(String seance) {
		this.seance = seance;
	}

	public Date getJour() {
		return jour;
	}

	public void setJour(Date jour) {
		this.jour = jour;
	}

	private static final long serialVersionUID = 1L;

	public Cour() {
		super();
	}

}
