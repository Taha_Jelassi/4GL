package tn.esprit.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: DateEnseignement
 *
 */
@Entity

public class DateEnseignement implements Serializable {

	@ManyToOne
	@JoinColumn(name = "idens", referencedColumnName = "idEns", updatable = false, insertable = false)
	private Enseignant ens;
	@ManyToOne
	@JoinColumn(name = "idecole", referencedColumnName = "idEcole", updatable = false, insertable = false)
	private Ecole ecole;
	@EmbeddedId
	private DateEnsPk dateenspk;

	public DateEnsPk getDateenspk() {
		return dateenspk;
	}

	public void setDateenspk(DateEnsPk dateenspk) {
		this.dateenspk = dateenspk;
	}

	public Enseignant getEns() {
		return ens;
	}

	public void setEns(Enseignant ens) {
		this.ens = ens;
	}

	public Ecole getEcole() {
		return ecole;
	}

	public void setEcole(Ecole ecole) {
		this.ecole = ecole;
	}

	private static final long serialVersionUID = 1L;

	public DateEnseignement() {
		super();
	}

}
