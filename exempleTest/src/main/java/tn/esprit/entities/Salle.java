package tn.esprit.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Salle
 *
 */
@Entity

public class Salle implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idSalle;
	private String description;
	@OneToOne
	private Enseignant ensgn;

	public Enseignant getEns() {
		return ensgn;
	}

	public void setEns(Enseignant ens) {
		this.ensgn = ens;
	}

	public long getIdSalle() {
		return idSalle;
	}

	public void setIdSalle(long idSalle) {
		this.idSalle = idSalle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private static final long serialVersionUID = 1L;

	public Salle() {
		super();
	}

}
