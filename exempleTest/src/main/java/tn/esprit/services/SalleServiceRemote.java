package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Salle;

@Remote
public interface SalleServiceRemote {

	public void addSalle(Salle s);

	public Salle updateSalle(Salle s);

	public Salle findSalleById(long id);

	public Salle findSalleByName(String name);

	public void deleteSalle(long id);

	public List<Salle> getAllSalle();
}
