package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Cour;
import tn.esprit.entities.Enseignant;

@Stateless
public class EnseignantServiceImpl implements EnseignantServiceLocal, EnseignantServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public void addEnseignant(Enseignant e) {
		// TODO Auto-generated method stub
		em.persist(e);
	}

	@Override
	public Enseignant updateEnseignant(Enseignant e) {
		// TODO Auto-generated method stub
		return em.merge(e);
	}

	@Override
	public Enseignant findEnseignantById(long id) {
		// TODO Auto-generated method stub
		return em.find(Enseignant.class, id);
	}

	@Override
	public Enseignant findEnseignantByName(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Enseignant> query = em.createQuery("select e from Enseignant e where e.name like :n",
				Enseignant.class);
		query.setParameter("n", name);
		return query.getSingleResult();
	}

	@Override
	public void deleteEnseignant(long id) {
		// TODO Auto-generated method stub
		em.remove(findEnseignantById(id));

	}

	@Override
	public List<Enseignant> getAllEnseignant() {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select e from Enseignant e");
		return query.getResultList();
	}
}
