package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Ecole;

@Remote
public interface EcoleServiceRemote {

	public void addEcole(Ecole s);

	public Ecole updateEcole(Ecole s);

	public Ecole findEcoleById(long id);

	public Ecole findEcoleByName(String name);

	public void deleteEcole(long id);

	public List<Ecole> getAllEcole();
}