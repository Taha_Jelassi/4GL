package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Cour;
import tn.esprit.entities.Ecole;

@Stateless
public class EcoleServiceImpl implements EcoleServiceLocal,EcoleServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public void addEcole(Ecole s) {
		// TODO Auto-generated method stub
		em.persist(s);
		
	}

	@Override
	public Ecole updateEcole(Ecole s) {
		// TODO Auto-generated method stub
		return em.merge(s);
	}

	@Override
	public Ecole findEcoleById(long id) {
		// TODO Auto-generated method stub
		 return em.find(Ecole.class, id);
	}

	@Override
	public Ecole findEcoleByName(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Ecole> query = em.createQuery("select c from Ecole c where c.nom like :n", Ecole.class);
		query.setParameter("n", name);
		return query.getSingleResult();
	}

	@Override
	public void deleteEcole(long id) {
		// TODO Auto-generated method stub
		em.remove(findEcoleById(id));
		
	}

	@Override
	public List<Ecole> getAllEcole() {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select e from Ecole");
		return query.getResultList();
	}
}
