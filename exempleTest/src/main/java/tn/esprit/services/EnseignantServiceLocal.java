package tn.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.entities.Enseignant;

@Local
public interface EnseignantServiceLocal {

	public void addEnseignant(Enseignant e);

	public Enseignant updateEnseignant(Enseignant e);

	public Enseignant findEnseignantById(long id);

	public Enseignant findEnseignantByName(String name);

	public void deleteEnseignant(long id);

	public List<Enseignant> getAllEnseignant();
}
