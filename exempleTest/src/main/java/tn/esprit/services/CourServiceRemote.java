package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Cour;

@Remote
public interface CourServiceRemote {

	public void addcour(Cour c);

	public Cour updateCour(Cour c);

	public Cour findCourById(long id);

	public Cour findCourByName(String name);

	public void deleteCour(long id);

	public List<Cour> getAllcour();

}
