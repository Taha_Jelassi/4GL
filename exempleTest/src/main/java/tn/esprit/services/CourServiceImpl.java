package tn.esprit.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Cour;

public class CourServiceImpl implements CourServiceLocal, CourServiceRemote {

	@PersistenceContext
	EntityManager em;

	@Override
	public void addcour(Cour c) {

		em.persist(c);
	}

	@Override
	public Cour updateCour(Cour c) {
		return em.merge(c);
	}

	@Override
	public Cour findCourById(long id) {
		return em.find(Cour.class, id);
	}

	@Override
	public Cour findCourByName(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Cour> query = em.createQuery("select c from Cour c where c.seance like :n", Cour.class);
		query.setParameter("n", name);
		return query.getSingleResult();
	}

	@Override
	public void deleteCour(long id) {
		// TODO Auto-generated method stub
		em.remove(findCourById(id));
	}

	@Override
	public List<Cour> getAllcour() {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select c from Cour c");
		return query.getResultList();
	}

}
